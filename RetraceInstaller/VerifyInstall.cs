﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Fabric;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Security.Principal;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;


namespace RetraceInstaller
{
    public class VerifyInstall
    {
        private StatelessServiceContext context;

        const string MachineRoot = "HKEY_Local_Machine";
        const string subkey1 = "SYSTEM\\CurrentControlSet\\Services\\StackifyHealthService";
        const string subkey2 = "SYSTEM\\CurrentControlSet\\Services\\StackifyMonitoringService";
        const string keyName1 = MachineRoot + "\\" + subkey1;
        const string keyName2 = MachineRoot + "\\" + subkey2;
        public VerifyInstall(StatelessServiceContext context)

        {
            this.context = context;
        }


        public bool VerifyRetraceInstallation(out string message)
        {
            message = "Retrace is NOT installed";
            if (IsRetraceInstalled())
            {
                message = "Retrace is installed";
                return true;
            }
            InstallRetrace();
            return false;
        }

        private void InstallRetrace()
        {
            var configurationPackage = context.CodePackageActivationContext.GetConfigurationPackageObject("Config");
            var retraceKey = configurationPackage.Settings.Sections["DataConfigurationSection"].Parameters["RetraceActivationKey"];

            var retraceSetupUri = Environment.GetEnvironmentVariable("RetraceSetupUri");
            var retraceEnvironment = Environment.GetEnvironmentVariable("RetraceEnvironment");
            var retraceEnableProfiler = Environment.GetEnvironmentVariable("RetraceEnableProfiler");
            var setupFilename = "StackifySetup.exe";
            var installParams = $"/s /v\"ACTIVATIONKEY={retraceKey.Value} ENVIRONMENT=\"{retraceEnvironment}\" ENABLEPROFILER={retraceEnableProfiler} ATTACHALL=1 /qn /l*v .\\Log.txt\"";

            WebClient wc = new WebClient();
            try
            {
                if(!IsUserAdministrator())
                    throw new Exception("Not an admin!!!");

                wc.DownloadFile(retraceSetupUri, setupFilename);
                ProcessStartInfo psi = new ProcessStartInfo
                {
                    Arguments = installParams,
                    CreateNoWindow = false,
                    FileName = setupFilename,
                    UseShellExecute = true
                };

                var process = Process.Start(psi);
                process.WaitForExit();
            }
            catch (Exception e)
            {
                ServiceEventSource.Current.ServiceMessage(context, e.Message);
            }
        }

        public bool IsRetraceInstalled()
        {
            var key = Registry.GetValue(keyName1, "DisplayName", null);
            if (key == null)
                return false;
            key = Registry.GetValue(keyName2, "DisplayName", null);
            return key != null;
        }

        public bool IsUserAdministrator()
        {
            //bool value to hold our return value
            bool isAdmin;
            WindowsIdentity user = null;
            try
            {
                //get the currently logged in user
                user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException ex)
            {
                isAdmin = false;
            }
            catch (Exception ex)
            {
                isAdmin = false;
            }
            finally
            {
                if (user != null)
                    user.Dispose();
            }
            return isAdmin;
        }

        public void WriteApplicationInformation()
        {
            var configurationPackage = context.CodePackageActivationContext.GetConfigurationPackageObject("Config");
            var connectionStringParameter = configurationPackage.Settings.Sections["DataConfigurationSection"].Parameters["StorageConnectionString"];
            try
            {
                var fc = new FabricClient();
                var appList = fc.QueryManager.GetDeployedApplicationListAsync(context.NodeContext.NodeName).GetAwaiter().GetResult();
                var jsonAppList = JsonConvert.SerializeObject(appList);

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionStringParameter.Value);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference("stackifyconfig");
                container.CreateIfNotExists();
                CloudBlockBlob blockBlob = container.GetBlockBlobReference($"{context.NodeContext.NodeName.ToLower()}-applist");
                blockBlob.UploadText(jsonAppList);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
    }
}
