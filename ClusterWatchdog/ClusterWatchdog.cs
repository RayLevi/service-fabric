﻿using System;
using System.Collections.Generic;
using System.Fabric;
using System.Linq;
using System.ServiceModel.Activation.Configuration;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceFabric.Services.Communication.Runtime;
using Microsoft.ServiceFabric.Services.Runtime;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ClusterWatchdog
{
    /// <summary>
    /// An instance of this class is created for each service instance by the Service Fabric runtime.
    /// </summary>
    internal sealed class ClusterWatchdog : StatelessService
    {
        public ClusterWatchdog(StatelessServiceContext context)
            : base(context)
        { }

        /// <summary>
        /// Optional override to create listeners (e.g., TCP, HTTP) for this service replica to handle client or user requests.
        /// </summary>
        /// <returns>A collection of listeners.</returns>
        protected override IEnumerable<ServiceInstanceListener> CreateServiceInstanceListeners()
        {
            return new ServiceInstanceListener[0];
        }

        /// <summary>
        /// This is the main entry point for your service instance.
        /// </summary>
        /// <param name="cancellationToken">Canceled when Service Fabric needs to shut down this service instance.</param>
        protected override async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following sample code with your own logic 
            //       or remove this RunAsync override if it's not needed in your service.

            long iterations = 0;

            TraceEventSamples.SimpleEventSourceMonitor.Run();

            while (true)
            {
                cancellationToken.ThrowIfCancellationRequested();
                try
                {
                    var configurationPackage = Context.CodePackageActivationContext.GetConfigurationPackageObject("Config");
                    var connectionStringParameter = configurationPackage.Settings.Sections["DataConfigurationSection"].Parameters["StorageConnectionString"];

                    ServiceEventSource.Current.ServiceMessage(this.Context, "Working-{0}", ++iterations);
                    var fc = new FabricClient();
                    var nodelist = fc.QueryManager.GetNodeListAsync().GetAwaiter().GetResult();
                    var jsonNodeList = JsonConvert.SerializeObject(nodelist);

                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionStringParameter.Value);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    CloudBlobContainer container = blobClient.GetContainerReference("stackifyconfig");
                    container.CreateIfNotExists();
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference("nodelist");
                    blockBlob.UploadText(jsonNodeList);

                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e);
                }

                await Task.Delay(TimeSpan.FromSeconds(60), cancellationToken);
            }
        }
    }
}
