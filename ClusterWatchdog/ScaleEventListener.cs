﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClusterWatchdog
{
    sealed class ScaleEventListener : EventListener
    {
        /// <summary> 
        /// Storage file to be used to write logs 
        /// </summary> 
        private string m_StorageFile = null;

        /// <summary> 
        /// Name of the current event listener 
        /// </summary> 
        private string m_Name;

        public ScaleEventListener(string name)
        {
            this.m_Name = name;

            Debug.WriteLine("StorageFileEventListener for {0} has name {1}", GetHashCode(), name);

            AssignLocalFile();
        }

        private async void AssignLocalFile()
        {
            //m_StorageFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(m_Name.Replace(" ", "_") + ".log",
            //  CreationCollisionOption.OpenIfExists);
        }

        private async void WriteToFile(IEnumerable<string> lines)
        {
            // TODO: 
        }

        protected override void OnEventWritten(EventWrittenEventArgs eventData)
        {
            // TODO: 
        }

        protected override void OnEventSourceCreated(EventSource eventSource)
        {
            // TODO: 
        }
    }
}